#!/usr/bin/env python
import argparse
from importlib import import_module
import sys
from os import listdir

parser = argparse.ArgumentParser(description='Multipurpose cipher encryption/decryption tool.')
parser.add_argument('-l', '--list', help='List all ciphers', action='store_true')
parser.add_argument('-r', '--regex', help='Search for ciphers beginning with search term', metavar=('search-term'))
parser.add_argument('-i', '--info', nargs=1, help='Get info on specific ciphers', metavar=('cipher'))
parser.add_argument('-e', '--encrypt', nargs='*', help='Encrypt text using cipher of choice', metavar=('cipher', 'ciphertext'))
parser.add_argument('-d', '--decrypt', nargs='*', help='Decrypt text using cipher of choice', metavar=('cipher', 'ciphertext'))

args = parser.parse_args()

def list_ciphers():
	ciphers=sorted(set([x.split('.')[0] for x in listdir('ciphers') if not x.startswith('_')]))
	return ciphers

def call_cipher(operation, ciphername, ciphertext, cipherarguments):
	try:
		cipher_obj = import_module('ciphers.'+ciphername)
	except ImportError as e:
		print "Could not find cipher python module: " + ciphername
		exit(1)
	except:
		raise

	operation = operation.lower()

	if operation[0] == 'e':
		print cipher_obj.encrypt(ciphertext, cipherarguments)

	elif operation[0] == 'd':
		print cipher_obj.decrypt(ciphertext, cipherarguments)

	else: print "Operation not implemented"

def info_cipher(ciphername):
	try:
		cipher_obj = import_module('ciphers.'+ciphername)
	except ImportError as e:
		print "Could not find cipher python module: " + ciphername
		exit(1)
	except:
		raise
	try:
		if len(args.info) == 1:
				try:
			  		print cipher_obj.info()
				except AttributeError:
			  		print "Module does not have an info() method"
	except:
		pass

def main():
	#Look for LIST argument and act on it
	try:
		if args.list:
			#Print all ciphers
			for c in list_ciphers():
				print ' - '+c
			return
	except:
		pass

	#Regex search
	try:
		if len(args.regex) == 1:
			for c in list_ciphers():
				if c.startswith(args.regex[0]):
					print ' - '+c
			return
	except:
		pass

	#Look for INFO argument and act on it
	try:
		info_cipher(args.info[0])
	except:
		pass

	#Look for ENCRYPT argument and act on it
	try:
		#Check for 2+ args for encrypt function
		if len(args.encrypt) >= 2:
			#Encrypt with cipher of choice
			call_cipher('e', args.encrypt[0], args.encrypt[1], args.encrypt[2:])
	except:
		pass

	#Look for DECRYPT argument and act on it
	try:
		#Check for 2+ args for decrypt function
		if len(args.decrypt) >= 2:
			#Decrypt with cipher of choice
			call_cipher('d', args.decrypt[0], args.decrypt[1], args.decrypt[2:])
	except:
		pass
	return

if __name__ == "__main__":
	main()

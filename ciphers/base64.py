import binascii
import string

## can't import modules with same name so copied b64encode functions from the source of the built in python library 'base64'
#Original arguments made altchars as None, I changed this to '' to suite with our import method

def b64encode(s, altchars=''):
    """Encode a string using Base64.

    s is the string to encode.  Optional altchars must be a string of at least
    length 2 (additional characters are ignored) which specifies an
    alternative alphabet for the '+' and '/' characters.  This allows an
    application to e.g. generate url or filesystem safe Base64 strings.

    The encoded string is returned.
    """
    # Strip off the trailing newline
    encoded = binascii.b2a_base64(s)[:-1]
    if altchars is not '':
        return encoded.translate(string.maketrans(b'+/', altchars[:2]))
    return encoded

def b64decode(s, altchars=''):
    """Decode a Base64 encoded string.

    s is the string to decode.  Optional altchars must be a string of at least
    length 2 (additional characters are ignored) which specifies the
    alternative alphabet used instead of the '+' and '/' characters.

    The decoded string is returned.  A TypeError is raised if s is
    incorrectly padded.  Characters that are neither in the normal base-64
    alphabet nor the alternative alphabet are discarded prior to the padding
    check.
    """
    if altchars is not '':
        s = s.translate(string.maketrans(altchars[:2], '+/'))
    try:
        return binascii.a2b_base64(s)
    except binascii.Error, msg:
        # Transform this exception for consistency
        raise TypeError(msg)

def info():
	return "I: Base64 encoder/decoder"
def test(s, argv=''):
	return "T: Base64 called, ciphertext: " + s + " Args: " + str(argv)

def encrypt(s, argv=''):
	return b64encode(s,argv)

def decrypt(s, argv=''):
	return b64decode(s,argv)

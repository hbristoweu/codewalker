def info():
	return "I: Morse Code encoder and decoder. Morse code takes in a series of dots and dashes which combinations corrosponding to a particular letter in the alphabet. e.g. A=.-  AND   B=-...   so on, so forth"
def test(s, argv=''):
	#ENCODE
	#./codewalker.py e morse "abcdefghijklmnopqrstuvyxyz0123456789"
	#DECODE
	#./codewalker.py d morse ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- -.-- -..- -.-- --.. ----- .---- ..--- ...-- ....- ..... -.... --... ---.. ----. "

	return "T: Morse called, ciphertext: " + s + " Args: " + str(argv)

CODE = {'A': '.-',     'B': '-...',   'C': '-.-.', 
        'D': '-..',    'E': '.',      'F': '..-.',
        'G': '--.',    'H': '....',   'I': '..',
        'J': '.---',   'K': '-.-',    'L': '.-..',
        'M': '--',     'N': '-.',     'O': '---',
        'P': '.--.',   'Q': '--.-',   'R': '.-.',
     	'S': '...',    'T': '-',      'U': '..-',
        'V': '...-',   'W': '.--',    'X': '-..-',
        'Y': '-.--',   'Z': '--..',
        
        '0': '-----',  '1': '.----',  '2': '..---',
        '3': '...--',  '4': '....-',  '5': '.....',
        '6': '-....',  '7': '--...',  '8': '---..',
        '9': '----.' 
        }

DECODE = {v: k for k, v in CODE.iteritems()}

def encrypt(s, argv=''):
	ciphertext=''
	for c in s:
		try:
			ciphertext += CODE[c.upper()]+" "
		except:
			#Skip if c isn't in morse dict
			pass
	return ciphertext

def decrypt(s, argv=''):
	ciphertext=''
	for c in s.split(' '):
		try:
			ciphertext += DECODE[c.upper()]
		except:
			#Skip if c isn't in morse dict
			pass
	return ciphertext

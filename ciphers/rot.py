import caesar
def test(s, argv=''):
	return "T: Caesar called, ciphertext: " + s + " Args: " + str(argv)
def encrypt(s, argv=''):
	return caesar.encrypt(s,argv)
def decrypt(s, argv=''):
	return caesar.decrypt(s,argv)

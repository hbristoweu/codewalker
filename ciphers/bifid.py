#!/usr/bin/python

# Notice to contributors: I feel there are too many loops in this file.
# It could be optimised heavily.

def info():
  return '''Bifid Cipher is a cipher which combines the Polybius square with transposition, and uses fractionation to achieve diffusion. 
It was invented around 1901 by Felix Delastelle.'''

def test(s, argv=''):
  return "T: Bifid called, ciphertext: " + s + " Args: " + str(argv)

symbolset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

# Retuns a 2d array (Polybius square), removing char sanchar from the 26 char symbolset
def polybius(sanschar):
  sym = symbolset.replace(sanschar, '')
  sq = [[None for _ in range(5)] for _ in range(5)]
  i = 0
  for x in range(5):
    for y in range(5):
      sq[x][y] = sym[i]
      i += 1
  return sq

# Returns a 2d array (Polybius square) created from 25 char string s
def custom_polybius(s):
  sq = [[None for _ in range(5)] for _ in range(5)]
  i = 0
  for x in range(5):
    for y in range(5):
      sq[x][y] = s[i]
      i += 1
  return sq

# Checks for duplicate characters in string s, returns a bool
def str_dup_check(s):
  for c in s:
    if s.count(c) > 1:
      return True
  return False

# Returns the coordinates as tuple for char c in square sq
def polybius_coordinates(sq, c):
  for x in range(5):
    for y in range(5):
      if sq[x][y] == c:
        return (x, y)

# Returns the char at coordinates in tuple tup in square sq
def polybius_char(sq, tup):
  for x in range(5):
    for y in range(5):
      if (x,y) == tup:
        return sq[x][y]

def encrypt(s, argv=''):
  # If argv[0] is a single char, call polybius()
  # If argv[0] is a unique-char string of len 25, call custom_polybius()
  # If argv is empty, use a j-polybius
  sq = None
  if len(argv) > 0:
    c = argv[0].upper()
    if len(c) == 1:
      sq = polybius(c)
    elif len(c) == 25 and not str_dup_check(c):
      sq = custom_polybius(c)
  else:
    sq = polybius('J')

  # Coordinise the message
  s = s.upper().replace(' ', '')
  s_coord = list()
  for c in s:
    s_coord.append(polybius_coordinates(sq, c))

  # Cat the x coords then the y into a string
  coord_row = ""
  for tup in s_coord:
    coord_row += str(tup[0])
  for tup in s_coord:
    coord_row += str(tup[1])
  
  # Increment in 2's over the new string, pulling coord pairs (i, i+1) for a new text
  ciphertext = ""
  for i in range(0,len(coord_row),2):
    ciphertext += polybius_char(sq ,( int(coord_row[i]), int(coord_row[i+1]) ) )

  return ciphertext

def decrypt(s, argv=''):

  sq = None
  if len(argv) > 0:
    c = argv[0].upper()
    if len(c) == 1:
      sq = polybius(c)
    elif len(c) == 25 and not str_dup_check(c):
      sq = custom_polybius(c)
  else:
    sq = polybius('J')

  # Coordinise the ciphertext string
  s = s.upper().replace(' ', '')
  c_coord = list()
  for c in s:
    c_coord += polybius_coordinates(sq, c)
  
  # Grab characters from sq using coords gained from ciphertext string coords
  plaintext = ""
  for i in range(0,len(c_coord)/2):
    plaintext += polybius_char(sq, (int(c_coord[i]), int(c_coord[i+len(c_coord)/2])))

  return plaintext


# UAEOLWRINS
#print encrypt("FLEEATONCE", ["BGWKZQPNDSIOAXEFCLUMTHYVR"])
#print decrypt("UAEOLWRINS", ["BGWKZQPNDSIOAXEFCLUMTHYVR"])

#print str(str_dup_check("qwert"))
#print str(str_dup_check("important"))

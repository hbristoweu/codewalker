#!/usr/bin/python
def info():
  return '''I: The rail fence cipher (also called a zigzag cipher) is a form of transposition cipher. 
    It derives its name from the way in which it is encoded.
    This rail fence implementation is of the zig-zag kind.
    The railfence must be >= 2 rails, given in param 4 (Optional) 
    The railfence can take an offset value (Skip a rail) given in param 5 (Optional)'''
def test(p, argv=''):
  print "T: Railfence"
  for i in range(2,len(p)):
    s = encrypt(p, [i])
    print s + "\n" + decrypt(s, [i]) + " "+ str(p == decrypt(s, [i])) + "\n--------------- " + str(i)

def generate_railseq(railcount):
  railseq = ""
  for i in range(railcount):
    railseq += str(i)
  for j in range(railcount-2):
    railseq += str(i - (j + 1))
  return railseq

def railpattern(railcount, c_len):
  seq = range(railcount)
  seq += seq[1:-1][::-1]
  return ((seq[i%len(seq)],i) for i in xrange(c_len))

# Argv
# [0] Rail count
# [1] Offset (Rail skip)
def encrypt(s, argv=''):
  
  ciphertext = ""
  o = 0
  railcount = 3
  railseq = ""

  # The replace statements can be removed for a wider symbol set
  s = s.lower().replace(' ','').replace('.','')
    
  if len(argv) > 0:
    if int(argv[0]) > 1: 
      railcount = int(argv[0])
    else: return -1	# A railseq of wiidth 1 is plaintext. Pointless.
    if len(argv) > 1:
      o = int(argv[1])
  
  pat = railpattern(railcount, len(s))
  return "".join((c for (k,c) in sorted(zip(pat, s))))

def decrypt(s, argv=''):

  plaintext = ""
  o = 0
  railcount = 3
  railseq = ""

  # The replace statements can be removed for a wider symbol set
  s = s.lower().replace(' ','').replace('.','')

  if len(argv) > 0:
    if int(argv[0]) > 1:
      railcount = int(argv[0])
    else: return -1     # A railseq of wiidth 1 is plaintext. Pointless.
    if len(argv) > 1:
      o = int(argv[1])

  idx = sorted(railpattern(railcount, len(s)))
  idx = ((i, j, ix) for ix, (i,j) in enumerate(idx))
  from operator import itemgetter as ig
  plaintext = (s[i] for r,c, i in sorted(idx, key=ig(1)))

  return "".join(plaintext)

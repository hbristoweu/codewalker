#Mappings of letters to ints and visa versa
L2I = dict(zip("ABCDEFGHIJKLMNOPQRSTUVWXYZ",range(26)))
I2L = dict(zip(range(26),"ABCDEFGHIJKLMNOPQRSTUVWXYZ"))

def test(s, argv=''):
	return "T: Caesar called, ciphertext: " + s + " Args: " + str(argv)

def encrypt(s, argv=''):
	key=int(argv[0])
	ciphertext=''
	for c in s.upper():
		if c.isalpha(): ciphertext += I2L[ (L2I[c] + key)%26 ]
		else: ciphertext += c
	return ciphertext

def decrypt(s, argv=''):
	key=int(argv[0])
	ciphertext=''
	for c in s.upper():
		if c.isalpha(): ciphertext += I2L[ (L2I[c] - key)%26 ]
		else: ciphertext += c
	return ciphertext

#print encrypt("helloworld!",[1])
#print decrypt("IFMMPXPSME!",[1])

#Mappings of letters to ints and visa versa
L2I = dict(zip("ABCDEFGHIJKLMNOPQRSTUVWXYZ",range(26)))
I2L = dict(zip(range(26),"ABCDEFGHIJKLMNOPQRSTUVWXYZ"))

def test(s, argv=''):
	return "T: Caesar called, ciphertext: " + s + " Args: " + str(argv)

def encrypt(s, argv=''):
	key=argv[0]
	while len(s) > len(key):
		key=key+key
	s=zip(s,key)
	ciphertext=''
	for c in s:
		if c[0].isalpha(): ciphertext += I2L[ (L2I[c[0].upper()] + L2I[c[1].upper()])%26 ]
		else: ciphertext += c[0]
	return ciphertext

def decrypt(s, argv=''):
	key=argv[0]
	while len(s) > len(key):
		key=key+key
	s=zip(s,key)
	ciphertext=''
	for c in s:
		if c[0].isalpha(): ciphertext += I2L[ (L2I[c[0].upper()] - L2I[c[1].upper()])%26 ]
		else: ciphertext += c[0]
	return ciphertext

#print encrypt("helloworld!",["pizza"])
#print decrypt("WMKKOLWQKD!",["pizza"])

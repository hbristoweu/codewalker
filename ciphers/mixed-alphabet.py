from collections import OrderedDict

def info():
	return "I: A keyword is used to shift the beginning of the alphabet using one of each of the characters in the keyword. The remaining letters of the alphabet are then appended to this squence for letters to be encoded."

def test(s, argv=''):
	#Test this function using:
	#ENCODE
	#./codewalker.py e mixed-alphabet "secret message to be encoded "the quick brown fox jumped over the lazy dog"
	#DECODE
	#./codewalker.py d mixed-alphabet "DUEPUV NUDDTCU VX HU UFEXQUQ" "the quick brown fox jumped over the lazy dog"
	

 	return "T: Mixed Alphabet called, ciphertext: " + s + " Args: " + str(argv)

def generateNormalAlphabet(argv):
	#Allow for custom alphabets to be used
	#TODO: doesn't work... for some reason the program just ends if you call the cipher with a custom alphabet. Possible to do with how codewalker.py passes args?
	if len(argv) > 1:
		alphabet=argv[1].upper()
	else: alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	print alphabet
	return alphabet

def generateMixedAlphabet(alphabet,argv=''):
	key=argv[0]
	mixedAlphabet=''
	#Make key have no repeats
	key="".join(OrderedDict.fromkeys(key)).upper()
	#Get start of mixed alphabet by adding the key with letters only in the specified alphabet.
	for l in key:
		if l in alphabet:
			mixedAlphabet+=l
	#Add rest of alphabet (excluding letters in the key) to the mixed alphabet
	for l in alphabet:
		if l not in key:
			mixedAlphabet+=l
	print mixedAlphabet
	return mixedAlphabet

def mapCipher(s, I2L, L2I,mixedAlphabet):
	#Loop through ciphertext and map each char to it's corrosponding letter
	ciphertext=''
	for c in s.upper():
		if c in mixedAlphabet:
			ciphertext += I2L[ L2I[c] ]
		else: ciphertext += c
	return ciphertext

def encrypt(s, argv=''):

	alphabet=generateNormalAlphabet(argv)
	mixedAlphabet=generateMixedAlphabet(alphabet,argv)

	#Make mappings with mixed alphabet
	#Normal alphabet to ciphered alphabet
	L2I = dict(zip(alphabet,range(len(alphabet))))
	I2L = dict(zip(range(len(mixedAlphabet)),mixedAlphabet))

	ciphertext=mapCipher(s,I2L,L2I,mixedAlphabet)
	return ciphertext


def decrypt(s, argv=''):

	alphabet=generateNormalAlphabet(argv)
	mixedAlphabet=generateMixedAlphabet(alphabet,argv)

	#Make mappings with mixed alphabet
	#Mixed alphabet to normal alphabet
	L2I = dict(zip(mixedAlphabet,range(len(mixedAlphabet))))
	I2L = dict(zip(range(len(alphabet)),alphabet))

	ciphertext=mapCipher(s,I2L,L2I,mixedAlphabet)
	return ciphertext


#!/usr/bin/python
# Line 34: This block is duplicated in dec - could be methodised
# decrypt: Could use more comments before its optimised to fuck
def info():
  return '''The Columnar Transposition Cipher places text horizontally into n-columns (e.g. A key of 132436 for 6 columns). The product text is formed reading the columns vertically in numeric order.'''
def test(s, argv=''):
  return "T: Columnar called, ciphertext: " + s + " Args: " + str(argv)



import random, string, re, math

pad_symbols = "%_-+~$!^&*><@="
L2I = dict(zip("ABCDEFGHIJKLMNOPQRSTUVWXYZ",range(26)))



def get_padsym(s):
  for c in pad_symbols:
    if c not in s:
      return c
      break

# This can be optimised heavily
def str_to_key(s):
  values = {}
  key = ""
  keycharval = 1
  for c in string.uppercase:
    for i in range(len(s)):
      if c == s[i]:
        values[i] = keycharval
        keycharval += 1
  for i in range(len(s)):
    key += str(values[i])
  return key

# argv[0] Column sequence, implicit column_count (len(key))
# argv[1] "no-pad" option, if present prevents padding
def encrypt(s, argv=''):
  
  # 632415
  pad = True
  key = "632415"

  if len(argv) > 0:
    if re.search('[a-zA-Z]', argv[0]) == None:
      key = argv[0]
    else:
      key = str_to_key(argv[0].upper())
    if len(argv) > 1:
      if argv[1] == "no-pad": pad = False

  s = s.upper().replace(' ','').replace('.','')

  # We always encrypt with padding, but we pad with a distinct symbol not present in the symbolset
  # Occuranes of this symbol, when removed from the ciphertext. yield the unpadded version of the cipher
  pad_sym = get_padsym(s)

  if (len(s)%len(key)) > 0:
     s += ''.join(pad_sym for i in range(len(key)-len(s)%len(key)))
  
  x = len(key)

  # Horizontal matrix of width x, transposed
  #   zip(*zip(*[iter(s)]*x))
  # Squash colummns into strings
  table = list(map("".join,zip(*zip(*[iter(s)]*x))))
  # Reorder columns by index using key as a reference
  ord_col = [table[key.index(str(c))] for c in range(1,x+1)]

  ciphertext = "".join(ord_col)
  #for c in t22: ciphertext += c

  #print "EVLNEACDTKESEAQROFOJDEECUWIREE\n" + ciphertext + " Pad: " + str(pad)

  # Replace the pad chars with random characters
  if pad:
    for i in range(ciphertext.count(pad_sym)):
      ciphertext = ciphertext.replace(pad_sym,random.choice(string.uppercase),1)
    return ciphertext
  else: 
    return ciphertext.replace(pad_sym,'')
    

def decrypt(s, argv=''):

  # Default Key 632415
  key = "632415"

  if len(argv) > 0:
    if re.search('[a-zA-Z]', argv[0]) == None:
      key = argv[0]
    else:
      key = str_to_key(argv[0].upper())

  s = s.upper().replace(' ','').replace('.','')

  pad_sym = get_padsym(s)

  col_count = len(key)
  row_count = int(math.ceil(len(s)/float(len(key))))
  pad_count = len(key)*row_count - len(s)

  #print s + "\nColumn count:\t" + str(len(key)) + "\nRow count:\t" + str(row_count) + "\nPad count:\t" + str(pad_count) + "\nUsing Key: " + key
  
  key_padded = key[len(key)-pad_count:]
  key_nopad = key[:len(key)-pad_count]

  columns = [""] * col_count

  for i in range(1,len(key)+1):
    for k in key:
      if int(k) == i:
        if k in key_padded:
          columns[i-1] = s[:row_count-1] + pad_sym
          s = s[row_count-1:]
        else:
          columns[i-1] = s[:row_count]
          s = s[row_count:]

  i = 0
  ord_column = [""] * col_count
  for idx in key:
    ord_column[i] = columns[int(idx)-1]
    i += 1

  plaintext = ""
  ord_char = zip(*ord_column)

  for r in ord_char:
    plaintext+= ''.join(r)

  return plaintext.replace(pad_sym,'')



#c1 = encrypt("WE ARE DISCOVERED. FLEE AT ONCE", ["632415"])
#c1 = encrypt("WE ARE DISCOVERED. FLEE AT ONCE", ["zEbrAs"])
#c2 = encrypt("WE ARE DISCOVERED. FLEE AT ONCE", ["632415", "no-pad"])
#c2 = encrypt("WE ARE DISCOVERED. FLEE AT ONCE", ["ZeBrAS", "no-pad"])
# EVLWACDTESEAEROFTDEEIWIREC
#print "C:" + c1 + "\nP:" + decrypt(c1, ["zEbras"]) + "\n---------"
#print "C:" + c2 + "\nP:" + decrypt(c2, ["zeBras"])

#print str_to_key("ZEBRAS") + " ? 632415"
#print str_to_key("STRIPE")
#print str_to_key("BLEEDS")

# Wikipedia example of Double-Columnar Transposition
# Should Yeild Ciphertext: CAEENSOIAEDRLEFWEDREEVTOC
#c22 = encrypt(encrypt("WE ARE DISCOVERED. FLEE AT ONCE", ["ZEBRAS", "no-pad"]), ["STRIPE", "no-pad"])
#print "C:" + c22

#print decrypt("ARESA SOSTH EYLOI IAIEP ENGDL LTAHT FATEN HMW", ["POTATO"])

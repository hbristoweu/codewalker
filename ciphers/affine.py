#!/usr/bin/python
from fractions import gcd as _gcd

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        return -1
    else:
        return x % m

def test(s, argv=''):
  return "T: Affine called, ciphertext: " + s + " Args: " + str(argv)

# argv:
# [0] - a value
# [1] - b value
def encrypt(s, argv=''):

  a = 5
  b = 8

  if len(argv) > 0:
    if _gcd(int(argv[0]), 26) == 1:
      a = int(argv[0])
    else:
      return -1
    if len(argv) > 1:
      b = int(argv[1])

  ciphertext = ""
  s = s.upper().replace(' ', '')

  for c in s:
    ciphertext += alphabet[(a * alphabet.index(c) + b ) % 26]

  return ciphertext

def decrypt(s, argv=''):

  a = 5
  b = 8

  if len(argv) > 0:
    if _gcd(argv[0], 26) == 1:
      a = modinv(int(argv[0]), 26)
    else:
      return -1
    if len(argv) > 1:
      b = int(argv[1])

  plaintext = ""
  s = s.upper().replace(' ', '')

  for c in s:
    plaintext += alphabet[modinv(a, 26) * (alphabet.index(c) - b ) % 26]

  return plaintext

#print encrypt("message", [5, 8])
#print decrypt("QCUUIMC", [5, 8])


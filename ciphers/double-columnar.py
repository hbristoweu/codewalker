#!/usr/bin/python
try:
  import columnar
except Exception as e:
  raise e

def info():
  return '''The ciphertext from one columnar transposition undergoes a second round of encryption.

Parameters for Encrypt and Decrypt:
 1. First Key
 2. Second Key
 3. 'no-pad' switch, indicating that random padding is not to be added to the product ciphertext.

When decrypting, Keys should be provided in the REVERSE ORDER to encryption, as they are processed in the order they are given
'''

def test(s, argv=''):
  return "T: Double-columnar called, ciphertext: " + s + " Args: " + str(argv)

# argv[0] First key
# argv[1] Second key
# argv[2] 'no-pad' flag (Optional)
def encrypt(s, argv=''):

  # Two keys are required, in argv0 and argv1
  if len(argv) > 2:
    if argv[2] == "no-pad":
      return columnar.encrypt(columnar.encrypt(s, [argv[0], argv[2]]), [argv[1], argv[2]])
    else:
      return -1
  elif len(argv) > 1:
    return columnar.encrypt(columnar.encrypt(s, [argv[0]] ), [argv[1]])
  return -1

# Note: Keys should be provided in the REVERSE ORDER to encryption, as they are processed in the order they are given
# argv[0] First key
# argv[1] Second key
# argv[2] 'no-pad' flag (Optional)
def decrypt(s, argv=''):

  if len(argv) > 2:
    if argv[2] == "no-pad":
      return columnar.decrypt(columnar.decrypt(s, [argv[0], argv[2]]), [argv[1], argv[2]])
    else:
      return -1
  elif len(argv) > 1:
    return columnar.decrypt(columnar.decrypt(s, [argv[0]]), [argv[1]])
  return -1


#print encrypt("WE ARE DISCOVERED. FLEE AT ONCE", ["zebras", "stripe", "no-pad"])
#CAEENSOIAEDRLEFWEDREEVTOC
#print decrypt("CAEENSOIAEDRLEFWEDREEVTOC", ["stripe", "zebras", "no-pad"])
#WEAREDISCOVEREDFLEEATONCE

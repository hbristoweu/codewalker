#!/usr/bin/python

import affine

def info():
  return "Atbash is a preconfiguration of Affine, wherein a and b are both set to 25."
def test(s, argv=''):
  return "T: Caesar called, ciphertext: " + s
def encrypt(s, argv=''):
  return affine.encrypt(s, [25, 25])
def decrypt(s, argv=''):
  return affine.decrypt(s, [25 ,25])

#print encrypt("Message")
#print decrypt("NVHHZTV")

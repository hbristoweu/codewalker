# CODEWALKER #

CODEWALKER (temp name) is a Python-based classical ciphertext utility, for encoding, decoding, batch-operations and wordlist searching.

### Primary Tasks ###

* Implement the modular design with basic tests on multiple ciphers
* Implement test framework
* Define convention guidelines for creating compatible ciphers
* Optimise everything
* Watch issue tracker for more goals

### Design ###

CODEWALKER is to be ran from the terminal, and is to be called with arguments specifying

1. To Encrypt / Decrypt
2. The cipher name
3. The ciphertext
4. Additional cipher parameters (e.g. Rotation for Caesar)

An example run of the program _may_ appear like so:

    $ ./codewalker -e caesar "Hello this is a test message" 13
    Uryyb guvf vf n grfg zrffntr
    $ ./codewalker -d caesar "Uryyb guvf vf n grfg zrffntr" 13
    Hello this is a test message

The primary design goal of CODEWALKER is to create a *plug-and-play* ciphertext utility, wherein simply dropping a .py file into the *ciphers* directory allows that cipher to then be used to encrypt or decrypt messages.

### Who do I talk to? ###

* Har or Jdon